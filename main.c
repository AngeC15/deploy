#include <stdio.h>
#include <string.h>
#include <x86intrin.h>
#include <unistd.h>
#include "papi.h"


#define VECSIZE 50000

#define NB_FOIS 10
#define MAX_EVENTS 128
#define MAX_RAPL_EVENTS 64


char events[MAX_EVENTS][BUFSIZ];
char filenames[MAX_EVENTS][BUFSIZ];
FILE *fff[MAX_EVENTS];
static int num_eventsR=0;
static int num_eventsC=0;

int main(int argc, char **argv)
{

  	//init PAPI
	int retval,cidR, cidC,rapl_cid=-1,numcmp,coretemp_cid=-1;
    int EventSetR = PAPI_NULL;
    int EventSetC = PAPI_NULL;
    long long valuesR[MAX_EVENTS];
    long long valuesC[MAX_EVENTS];
    int code,enum_retval;
    const PAPI_component_info_t *cmpinfo = NULL;
	PAPI_event_info_t info;
    long long start_time,before_time,after_time;
    double elapsed_time,total_time;
    char event_namesRAPL[MAX_RAPL_EVENTS][PAPI_MAX_STR_LEN];
    char event_namesCoretemp[MAX_RAPL_EVENTS][PAPI_MAX_STR_LEN];

	char units[MAX_RAPL_EVENTS][PAPI_MIN_STR_LEN];
  	// Initialize the PAPI library
  	retval = PAPI_library_init(PAPI_VER_CURRENT);
  	if (retval != PAPI_VER_CURRENT && retval > 0) {
		fprintf(stderr,"PAPI library version mismatch!\n");
		exit(1);
	}
	if (retval < 0) {
		fprintf(stderr, "Initialization error!\n");
		exit(1);
	}

	//return the number of components currently installed
	numcmp = PAPI_num_components();

	//for each compononent, check if it's rapl and get the CID
	for(cidR=0; cidR<numcmp; cidR++) {

		if ( (cmpinfo = PAPI_get_component_info(cidR)) == NULL) {
			fprintf(stderr,"PAPI_get_component_info failed\n");
			exit(1);
		}

		//try to found rapl
		if (strstr(cmpinfo->name,"rapl")) {
			rapl_cid=cidR;
			printf("Found rapl component at cid %d\n", rapl_cid);

			if (cmpinfo->disabled) {
				fprintf(stderr,"No rapl events found: %s\n", cmpinfo->disabled_reason);

			}
            break;
		}

    }
    //for each compononent, check if it's rapl and get the CID
	for(cidC=0; cidC<numcmp; cidC++) {

		if ( (cmpinfo = PAPI_get_component_info(cidC)) == NULL) {
			fprintf(stderr,"PAPI_get_component_info failed\n");
			exit(1);
		}

		//try to found rapl
		if (strstr(cmpinfo->name,"coretemp")) {
			coretemp_cid=cidC;
			printf("Found coretemp component at cid %d\n", coretemp_cid);

			if (cmpinfo->disabled) {
				fprintf(stderr,"No coretemp events found: %s\n", cmpinfo->disabled_reason);

			}
            break;
		}

    }
     
    if (cidR==numcmp) {
		fprintf(stderr,"No components RAPL found\n");
		//don't interrupt the program to let computing 
    	//exit(1);
    }
    if (cidC==numcmp) {
		fprintf(stderr,"No components Coretemp found\n");
		//don't interrupt the program to let computing 
    	//exit(1);
    }
     
	retval = PAPI_create_eventset( &EventSetR );
    if (retval != PAPI_OK) {
    	fprintf(stderr,"Error creating rapl eventset!\n");
    }
    

    code = PAPI_NATIVE_MASK;

    //for rapl
    //enum_retval = PAPI_enum_cmp_event( &code, PAPI_ENUM_FIRST, cid );
	enum_retval = PAPI_enum_cmp_event( &code, PAPI_ENUM_FIRST, rapl_cid );
    
	while ( enum_retval == PAPI_OK) {
		//get the name in event_name with the code
    	retval = PAPI_event_code_to_name( code, event_namesRAPL[num_eventsR] );
    	//in case of error
		if ( retval != PAPI_OK ) {
	  		printf("Error translating %#x\n",code);
	  		exit(1);
		}
		retval = PAPI_get_event_info(code,&info);
		if (retval != PAPI_OK) {
	  		printf("Error while trying to get info about the code\n");
		
		}
	
		strncpy(units[num_eventsR],info.units,sizeof(units[0]));
		//buffer must be null terminated to safely use strstr operation on it below
		units[num_eventsR][sizeof(units[0])-1] = '\0';

		//add event
		retval = PAPI_add_event(EventSetR, code);
		if ( retval != PAPI_OK ) {
	  		printf("Error can't add the rapl event\n");
	  		break;
		}
		num_eventsR++;
		//get the next event in rapl and when finish, in coretemp
    	enum_retval = PAPI_enum_cmp_event( &code, PAPI_ENUM_EVENTS, cidR );
    }
    

/*
    code = PAPI_NATIVE_MASK;
    //for coretemp
	enum_retval = PAPI_enum_cmp_event( &code, PAPI_ENUM_FIRST, coretemp_cid );
    
	while ( enum_retval == PAPI_OK) {
		//get the name in event_name with the code
    	retval = PAPI_event_code_to_name( code, event_namesCoretemp[num_eventsC] );
    	//in case of error
		if ( retval != PAPI_OK ) {
	  		printf("Error translating %#x\n",code);
	  		exit(1);
		}
		retval = PAPI_get_event_info(code,&info);
		if (retval != PAPI_OK) {
	  		printf("Error while trying to get info about the code\n");
		
		}
	
		strncpy(units[num_eventsC],info.units,sizeof(units[0]));
		//buffer must be null terminated to safely use strstr operation on it below
		units[num_eventsC][sizeof(units[0])-1] = '\0';

		//add event
		retval = PAPI_add_event(EventSetC, code);
		if ( retval != PAPI_OK ) {
	  		printf("Error can't add the coretemp event\n");
	  		break;
		}
		num_eventsC++;
		//get the next event in rapl and when finish, in coretemp
    	enum_retval = PAPI_enum_cmp_event( &code, PAPI_ENUM_EVENTS, cidC );
    }
    */

  	//if no events were found:
	if(num_eventsR == 0){
		printf("Error, no event were found\n");
		//don't interrupt the program to let computing 
    	//exit(1);
	}
	 
	//opening the file to write data
	FILE* fptData ;
	fptData = fopen( "./out/datas.csv", "w+");
	if (fptData==NULL){
        printf("\nCan't open the file\n");
        exit(1);
    }
	//get the time of starting:
	start_time=PAPI_get_real_nsec();
	before_time=PAPI_get_real_nsec();
    retval = PAPI_start( EventSetR);
    if (retval != PAPI_OK) {
		fprintf(stderr,"PAPI_start() rapl failed\n");
		//don't interrupt the program to let computing 
    	//exit(1);
    }
   
	double oldPackageNrj = 0;
	double oldDramNrj = 0;
	double oldPp0Nrj = 0;
  	for (int i = 0; i < NB_FOIS; i++){

        
		//TODO: Read and reset the counter
		if(PAPI_read(EventSetR, valuesR) != PAPI_OK){
			printf("Error while reading RAPL values\n");
			//don't interrupt the program to let computing 
    		//exit(1);
		}
        if(PAPI_read(EventSetC, valuesC) != PAPI_OK){
			printf("Error while reading CoreTemp values\n");
			//don't interrupt the program to let computing 
    		//exit(1);
		}

	
		//write in the CSV:
		long long tmpTime = PAPI_get_real_nsec();
		double packageNrj = 0;
		double dramNrj = 0;
		double pp0Nrj = 0;
		double temp = 0;
		for(int i = 0; i < num_eventsR; i++){
			if (strstr(units[i],"nJ")) {
				if(strstr(event_namesRAPL[i], "PACKAGE_ENERGY:PACKAGE")){
					printf("Package energy found\n");
					packageNrj += (double)valuesR[i]/1.0e9;
				}else if(strstr(event_namesRAPL[i], "DRAM_ENERGY:PACKAGE")){
					printf("DRAM energy found\n");
					dramNrj += (double)valuesR[i]/1.0e9;
				}else if (strstr(event_namesRAPL[i], "PACKAGE_ENERGY:PACKAGE")){
					printf("PP0 energy found\n");
					pp0Nrj += (double)valuesR[i]/1.0e9;
				}
				
			}
		}
        code = PAPI_NATIVE_MASK;
        //for coretemp
        enum_retval = PAPI_enum_cmp_event( &code, PAPI_ENUM_FIRST, coretemp_cid );
        
        while ( enum_retval == PAPI_OK) {
            //get the name in event_name with the code
            retval = PAPI_event_code_to_name( code, event_namesCoretemp[num_eventsC] );
            //in case of error
            if ( retval != PAPI_OK ) {
                printf("Error translating %#x\n",code);
                exit(1);
            }
            retval = PAPI_get_event_info(code,&info);
            if (retval != PAPI_OK) {
                printf("Error while trying to get info about the code\n");
            
            }
            if (strstr(event_namesCoretemp,"temp")) {
                    /* Only print inputs */
                if (strstr(event_namesCoretemp,"_input")) {
                    retval = PAPI_create_eventset( &EventSetC );
                    if (retval != PAPI_OK) {
                        fprintf(stderr,"Error creating coretemp eventset!\n");
                    }
                    //add event
                    retval = PAPI_add_event(EventSetC, code);
                    if ( retval != PAPI_OK ) {
                        printf("Error can't add the coretemp event\n");
                        break;
                    }
                    retval = PAPI_start( EventSetC);
                    if (retval != PAPI_OK) {
                        fprintf(stderr,"PAPI_start() coretemp failed\n");
                        //don't interrupt the program to let computing 
                        //exit(1);
                    }
                    retval = PAPI_stop( EventSetC, valuesC);
                    if (retval != PAPI_OK) {
                        fprintf(stderr, "PAPI_stop() Coretemp failed\n");
                    }
                    temp=(valuesC[0]/1000.0);
                    retval = PAPI_cleanup_eventset( EventSetC );
                    if (retval != PAPI_OK) {
                        printf("Can't clear the coretemp eventSet\n");
                    }

                    retval = PAPI_destroy_eventset( &EventSetC );
                        if (retval != PAPI_OK) {
                            printf("Can't destroy the coretemp eventSet\n");
                    }
                }
            }
            enum_retval = PAPI_enum_cmp_event( &code, PAPI_ENUM_EVENTS, coretemp_cid );

        }


		
		//if(i%5 == 0){
			//timestamps
			fprintf(fptData,"%f,%f,%f,%f,%f\n", 
			tmpTime/1.0e9,packageNrj-oldPackageNrj,dramNrj-oldDramNrj,pp0Nrj-oldPp0Nrj,temp);
			//Package_Energy:Package0+1
			//fprintf(fptRAPL,"%f,", packageNrj-oldPackageNrj);
			//DRAM_Energy:Package0+1
			//fprintf(fptRAPL,"%f,", dramNrj-oldDramNrj);
			//PP0_Energy:Package0+1
			//fprintf(fptRAPL,"%f\n", pp0Nrj-oldPp0Nrj);
			oldPackageNrj = packageNrj;
			oldDramNrj = dramNrj;
			oldPp0Nrj = pp0Nrj;

		//}
	


		//printf("mncblas_sdot %d : res = %3.2f nombre de cycles: %Ld \n", i, res, end - start);
  	}

	printf("\n");
  	for (int i = 0; i < NB_FOIS; i++){

        
		
		//TODO: Read and reset the counter
		if(PAPI_read(EventSetR, valuesR) != PAPI_OK){
			printf("Error while reading RAPL values\n");
			//don't interrupt the program to let computing 
    		//exit(1);
		}
		if(PAPI_read(EventSetC, valuesC) != PAPI_OK){
			printf("Error while reading CoreTemp values\n");
			//don't interrupt the program to let computing 
    		//exit(1);
		}

	
		//write in the CSV:
		long long tmpTime = PAPI_get_real_nsec();
		double packageNrj = 0;
		double dramNrj = 0;
		double pp0Nrj = 0;
		double temp = 0;
		for(int i = 0; i < num_eventsR; i++){
			if (strstr(units[i],"nJ")) {
				if(strstr(event_namesRAPL[i], "PACKAGE_ENERGY:PACKAGE")){
					printf("Package energy found\n");
					packageNrj += (double)valuesR[i]/1.0e9;
				}else if(strstr(event_namesRAPL[i], "DRAM_ENERGY:PACKAGE")){
					printf("DRAM energy found\n");
					dramNrj += (double)valuesR[i]/1.0e9;
				}else if (strstr(event_namesRAPL[i], "PACKAGE_ENERGY:PACKAGE")){
					printf("PP0 energy found\n");
					pp0Nrj += (double)valuesR[i]/1.0e9;
				}
				
			}
		}
        code = PAPI_NATIVE_MASK;
        //for coretemp
        enum_retval = PAPI_enum_cmp_event( &code, PAPI_ENUM_FIRST, coretemp_cid );
        
        while ( enum_retval == PAPI_OK) {
            //get the name in event_name with the code
            retval = PAPI_event_code_to_name( code, event_namesCoretemp[num_eventsC] );
            //in case of error
            if ( retval != PAPI_OK ) {
                printf("Error translating %#x\n",code);
                exit(1);
            }
            retval = PAPI_get_event_info(code,&info);
            if (retval != PAPI_OK) {
                printf("Error while trying to get info about the code\n");
            
		    }   
            if (strstr(event_namesCoretemp,"temp")) {
                    /* Only print inputs */
                if (strstr(event_namesCoretemp,"_input")) {
                    retval = PAPI_create_eventset( &EventSetC );
                    if (retval != PAPI_OK) {
                        fprintf(stderr,"Error creating coretemp eventset!\n");
                    }
                    //add event
                    retval = PAPI_add_event(EventSetC, code);
                    if ( retval != PAPI_OK ) {
                        printf("Error can't add the coretemp event\n");
                        break;
                    }
                    retval = PAPI_start( EventSetC);
                    if (retval != PAPI_OK) {
                        fprintf(stderr,"PAPI_start() coretemp failed\n");
                        //don't interrupt the program to let computing 
                        //exit(1);
                    }
                    retval = PAPI_stop( EventSetC, valuesC);
                    if (retval != PAPI_OK) {
                        fprintf(stderr, "PAPI_stop() Coretemp failed\n");
                    }
                    temp=(valuesC[0]/1000.0);
                    retval = PAPI_cleanup_eventset( EventSetC );
                    if (retval != PAPI_OK) {
                        printf("Can't clear the coretemp eventSet\n");
                    }

                    retval = PAPI_destroy_eventset( &EventSetC );
                    if (retval != PAPI_OK) {
                        printf("Can't destroy the coretemp eventSet\n");
                    }
                }
            }
            enum_retval = PAPI_enum_cmp_event( &code, PAPI_ENUM_EVENTS, coretemp_cid );


        }
		
		//if(i%5 == 0){
			//timestamps
			fprintf(fptData,"%f,%f,%f,%f,%f\n", 
			tmpTime/1.0e9,packageNrj-oldPackageNrj,dramNrj-oldDramNrj,pp0Nrj-oldPp0Nrj,temp);
			//Package_Energy:Package0+1
			//fprintf(fptRAPL,"%f,", packageNrj-oldPackageNrj);
			//DRAM_Energy:Package0+1
			//fprintf(fptRAPL,"%f,", dramNrj-oldDramNrj);
			//PP0_Energy:Package0+1
			//fprintf(fptRAPL,"%f\n", pp0Nrj-oldPp0Nrj);
			oldPackageNrj = packageNrj;
			oldDramNrj = dramNrj;
			oldPp0Nrj = pp0Nrj;

		//}


    	//printf("mncblas_cdotu_sub %d : res = %3.2f +i %3.2f nombre de cycles: %Ld \n", i, resComplexe.real, resComplexe.imaginary, end - start);

  	}
	fclose(fptData);
	printf("\n");
	printf("=======================================================\n");

	after_time=PAPI_get_real_nsec();
    retval = PAPI_stop( EventSetR, valuesR);
    if (retval != PAPI_OK) {
        fprintf(stderr, "PAPI_stop() RAPL failed\n");
    }

	total_time=((double)(after_time-start_time))/1.0e9;
    elapsed_time=((double)(after_time-before_time))/1.0e9;
	for(int i=0;i<num_eventsR;i++) {
		printf("%.4f %.1f (* Average Power for %s *)\n",total_time,((double)valuesR[i]/1.0e9)/elapsed_time,events[i]);
    }
    printf("=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+\n");
    for(int i=0;i<num_eventsC;i++) {
		printf("%.4f %.1f (* Average Temp for %s *)\n",total_time,((double)valuesC[i]/1000));
    }

	printf("\n");

    return 0;
   
}
