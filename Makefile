

all: main main2

INC_DIR=../include
INC_PAPI=/home/cpelisseverdoux/papi

PAPI_HOME=/home/cpelisseverdoux/papi
PAPI_INCLUDE = $(PAPI_HOME)/include
PAPI_LIBRARY = -L$(PAPI_HOME)/lib -lpapi -Wl,-rpath,$(PAPI_HOME)/lib


LIB_DIR=../lib

LIBST=-lmnblas
LIBDYN=-lmnblasdyn

OPTIONS_COMPIL  = -g -Wall -O2 -fPIC -I$(INC_DIR) -lm

OPTIONS_LINK_STATIC  =   -fopenmp -L$(LIB_DIR) -L$(INC_PAPI)/lib 
OPTIONS_LINK_DYN  = -fopenmp -L$(LIB_DIR)

main: main.o
	gcc -o main main.o $(OPTIONS_LINK_STATIC) $(PAPI_LIBRARY)

main.o: main.c
	gcc $(OPTIONS_COMPIL) -I$(PAPI_INCLUDE) -c main.c 

main2: main2.o
	gcc -o main2 main2.o $(OPTIONS_LINK_STATIC) $(PAPI_LIBRARY)

main2.o: main2.c
	gcc $(OPTIONS_COMPIL) -I$(PAPI_INCLUDE) -c main2.c 


clean:
	rm -f *.o main main2